#! Python3

import subprocess
import argparse
import sys
import os





def createArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--OpenShift',  action='store_true', default='false', help='поиск OpenShift')
    parser.add_argument('-cl', '--CloseShift', action='store_true', default='false', help='поиск CloseShift')
    parser.add_argument('-t', '--ticketNum', type=str, default=-1, help='ввести номер месячного билета "-t00000***"  (если ввести 0-выдаст все билеты)')
    parser.add_argument('-er', '--error_code', type=str, default=-1, help='ввести -er"пробел код ошибки"')
    parser.add_argument('-c', '--CuState', type=int, default=0, help='поиск CuState !=1')
    parser.add_argument('-f', '--FreeCellsCount', type=int, default=10992, help='поиск FreeCellsCount=10992')

    return parser


parser = createArgParser()
namespace = parser.parse_args()
TMP = 'C:/Users/n007/.PyCharmCE2018.3/config/scratches/12.txt'
TMP_result = 'C:/Users/n007/.PyCharmCE2018.3/config/scratches/TMP_result.txt'
r = open(TMP, 'w')
result = open(TMP_result, 'w')
r.close()
result.close()

from tkinter.filedialog import askopenfilename
filename = askopenfilename()

#Функция поиска в логе команды открытия смены
def OpenShift():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    #Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        #если не нашли нужный текст в строке ищем дальше
        if oneline.find('<Response><Command>OpenShift') == -1:
            prev_string_oneline = oneline
            continue
        #если нашли
        else:
            #открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result =  open(TMP_result, 'a')
            #копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()

    #парсим XML по тегам и записываем результат в файл
    r = open(TMP, 'r')
    result = open(TMP_result, 'a')
    import xml.etree.ElementTree as ET
    tree = ET.ElementTree(file=TMP)
    for t in tree.iterfind('.//'):
        temp = ('%s = %s' % (t.tag, t.text))
        result.write(temp + '\n')
    r.close()
    result.close()


def CloseShift():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        if oneline.find('<Response><Command>CloseShift') == -1:
            prev_string_oneline = oneline
            continue
        # если нашли
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()

    # парсим XML по тегам и записываем результат в файл
    r = open(TMP, 'r')
    result = open(TMP_result, 'a')
    import xml.etree.ElementTree as ET
    tree = ET.ElementTree(file=TMP)
    for t in tree.iterfind('.//'):
        temp = ('%s = %s' % (t.tag, t.text))
        result.write(temp + '\n')
    r.close()
    result.close()


def get_ticketNum():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.ticketNum
        if oneline.find('<PreviewMode>') == -1:
            prev_string_oneline = oneline
            continue
        else:
            if oneline.find('<TicketNumMN>' + str(numbers)) == -1:
                prev_string_oneline = oneline
                continue
            else:
                # открываем текстовые файлы для вывода резельтатов поиска
                r = open(TMP, 'w')
                result = open(TMP_result, 'a')
                # копируем найденную строку в нужный файл
                result.write(prev_string_oneline + '\n')
                r.write(oneline + '\n' + '\n')
                r.close()
                result.close()
                # парсим XML по тегам и записываем результат в файл
                r = open(TMP, 'r', encoding="windows-1251")
                result = open(TMP_result, 'a', encoding="windows-1251")
                from lxml import etree as ET
                tree = ET.parse(r) # Парсинг строки
                for t in tree.iterfind('.//'):  # поиск элементов
                    print(t.get('name'))
                    temp = ('%s = %s' % (t.tag, t.text))
                    result.write(temp + '\n')
                r.close()
                result.close()


def get_error_code():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.error_code
        if oneline.find('<ErrorCode>' + str(numbers)) == -1:
            prev_string_oneline = oneline
            continue
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            # парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r', encoding="windows-1251")
            result = open(TMP_result, 'a', encoding="windows-1251")
            from lxml import etree as ET
            tree = ET.parse(r) # Парсинг строки
            for t in tree.iterfind('.//'):  # поиск элементов
                print(t.get('name'))
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()



if  namespace.OpenShift == True:
    OpenShift()

if namespace.CloseShift == True:
    CloseShift()

if namespace.ticketNum != 0:
    numbers = namespace.ticketNum
    get_ticketNum()

if namespace.error_code != 0:
    numbers = namespace.ticketNum
    get_error_code()



if namespace.FreeCellsCount == True:
    print(3)

from os import startfile
startfile(TMP_result)

