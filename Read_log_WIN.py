#! Python3
# -*- coding: UTF-8 -*-

import argparse
import os
import gzip

def createArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--OpenShift',  action='store_true', default='false', help='поиск OpenShift/CloseShift')
    parser.add_argument('-t', '--ticketNum', type=str, default=-1, help='ввести номер месячного билета "-t00000***"  (если ввести 0-выдаст все билеты)')
    parser.add_argument('-er', '--error_code', type=str, default=-1, help='ввести -er"пробел код ошибки"')
    parser.add_argument('-c', '--CuState', action='store_true', default='false', help='поиск CuState !=0, ввод -c без параметров(+ время открытия и закрытия смены)')
    parser.add_argument('-f', '--FreeCellsCount', action='store_true', default='false', help='поиск FreeCellsCount=10992')

    return parser

parser = createArgParser()
namespace = parser.parse_args()
TMP = (os.getcwd() + "TMP.txt")
TMP_result = (os.getcwd() + "TMP_result.txt")
TMP_arch = (os.getcwd() + "TMP_arch.txt")
r = open(TMP, 'w')
result = open(TMP_result, 'w')
arch = open(TMP_arch, 'w')
r.close()
result.close()
arch.close()

from tkinter.filedialog import askopenfilename
filename = askopenfilename()

if filename.split(".")[3] == "gz":
   f = gzip.open(filename, 'wt', encoding="UTF-8")
   f.write(TMP_arch)
   f.close()
   filename = TMP_arch
else:
    print('NOt Arch')



#Функция поиска в логе команды открытия смены
def OpenShift():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    #Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        #если не нашли нужный текст в строке ищем дальше
        if oneline.find('<Response><Command>OpenShift') == -1 and oneline.find('<Response><Command>CloseShift') == -1:
            prev_string_oneline = oneline
            continue
        #если нашли
        else:
            #открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result =  open(TMP_result, 'a')
            #копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            #парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r')
            result = open(TMP_result, 'a')
            import xml.etree.ElementTree as ET
            tree = ET.ElementTree(file=TMP)
            for t in tree.iterfind('.//'):
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()

def get_ticketNum():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.ticketNum
        if oneline.find('<PreviewMode>') == -1:
            prev_string_oneline = oneline
            continue
        else:
            if oneline.find('<TicketNumMN>' + str(numbers)) == -1:
                prev_string_oneline = oneline
                continue
            else:
                # открываем текстовые файлы для вывода резельтатов поиска
                r = open(TMP, 'w')
                result = open(TMP_result, 'a')
                # копируем найденную строку в нужный файл
                result.write(prev_string_oneline + '\n')
                r.write(oneline + '\n' + '\n')
                r.close()
                result.close()
                # парсим XML по тегам и записываем результат в файл
                r = open(TMP, 'r', encoding="windows-1251")
                result = open(TMP_result, 'a', encoding="windows-1251")
                from lxml import etree as ET
                tree = ET.parse(r) # Парсинг строки
                for t in tree.iterfind('.//'):  # поиск элементов
                    print(t.get('name'))
                    temp = ('%s = %s' % (t.tag, t.text))
                    result.write(temp + '\n')
                r.close()
                result.close()

def get_error_code():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.error_code
        if oneline.find('<ErrorCode>' + str(numbers)) == -1:
            prev_string_oneline = oneline
            continue
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            # парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r', encoding="windows-1251")
            result = open(TMP_result, 'a', encoding="windows-1251")
            from lxml import etree as ET
            tree = ET.parse(r) # Парсинг строки
            for t in tree.iterfind('.//'):  # поиск элементов
                print(t.get('name'))
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()

def get_CuState():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.error_code
        if oneline.find('<Response><Command>OpenShift') == -1 and oneline.find('<Response><Command>CloseShift') == -1 and oneline.find('<CuState>2<') == -1 and oneline.find('<CuState>1<') == -1:
            prev_string_oneline = oneline
            continue
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            # парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r', encoding="windows-1251")
            result = open(TMP_result, 'a', encoding="windows-1251")
            from lxml import etree as ET
            tree = ET.parse(r) # Парсинг строки
            for t in tree.iterfind('.//'):  # поиск элементов
                print(t.get('name'))
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()

def get_FreeCellsCount():
        prev_string_oneline = ''
        f = open(filename, 'r', encoding="UTF-8")
        # Цикл построчного поиска нужной команды
        for oneline in f:
            oneline = oneline.strip(' \t\n\r ')
            # если не нашли нужный текст в строке ищем дальше
            numbers = namespace.error_code
            if oneline.find('<FreeCellsCount>10992<') == -1:
                prev_string_oneline = oneline
                continue
            else:
                # открываем текстовые файлы для вывода резельтатов поиска
                r = open(TMP, 'w')
                result = open(TMP_result, 'a')
                # копируем найденную строку в нужный файл
                result.write(prev_string_oneline + '\n')
                r.write(oneline + '\n' + '\n')
                r.close()
                result.close()
                # парсим XML по тегам и записываем результат в файл
                r = open(TMP, 'r', encoding="windows-1251")
                result = open(TMP_result, 'a', encoding="windows-1251")
                from lxml import etree as ET
                tree = ET.parse(r)  # Парсинг строки
                for t in tree.iterfind('.//'):  # поиск элементов
                    print(t.get('name'))
                    temp = ('%s = %s' % (t.tag, t.text))
                    result.write(temp + '\n')
                r.close()
                result.close()


if namespace.OpenShift == True:
    OpenShift()

if namespace.ticketNum != 0:
    numbers = namespace.ticketNum
    get_ticketNum()

if namespace.error_code != 0:
    numbers = namespace.ticketNum
    get_error_code()

if namespace.CuState == True:
    get_CuState()

if namespace.FreeCellsCount == True:
    get_FreeCellsCount()

from os import startfile
startfile(TMP_result)

