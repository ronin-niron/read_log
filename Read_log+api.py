#! Python3
# -*- coding: UTF-8 -*-

import argparse
import os
import gzip



def createArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--OpenShift',  action='store_true', default='false', help='поиск OpenShift/CloseShift')
    parser.add_argument('-t', '--ticketNum', type=str, default=0, help='ввести номер месячного билета "-t00000***"  (если ввести 0-выдаст все билеты)')
    parser.add_argument('-er', '--error_code', type=str, default=0, help='ввести -er"пробел код ошибки"')
    parser.add_argument('-c', '--CuState', action='store_true', default='false', help='поиск CuState !=0, ввод -c без параметров(+ время открытия и закрытия смены)')
    parser.add_argument('-f', '--FreeCellsCount', action='store_true', default='false', help='поиск FreeCellsCount=10992')
    parser.add_argument('-x', '--Test', action='store_true', default='false', help='поиск OpenShift/CloseShift + api_log')

    return parser

parser = createArgParser()
namespace = parser.parse_args()
TMP = (os.getcwd() + '\\' + "TMP.txt")
TMP_result = (os.getcwd() + '\\' + "TMP_result.txt")
TMP_arch = (os.getcwd() + '\\' + "TMP_arch.txt")
arch = open(TMP_arch, 'w')
r = open(TMP, 'w')
result = open(TMP_result, 'w')
r.close()
result.close()
arch.close()


from tkinter.filedialog import askopenfilename
filename = askopenfilename()

from tkinter.filedialog import askopenfilename
filename_2 = askopenfilename()

try:
    if filename.split(".")[3] == "gz" :
       f = gzip.open(filename, 'rt', encoding="UTF-8")
       t = open(TMP_arch, 'a', encoding="UTF-8")
       fo = f.read()
       t.write(fo)
       f.close()
       t.close()
       filename = TMP_arch
    else:
        print('', end='')
except Exception:
    print('Выбранный файл не является архивом')
finally:
    print('', end='')

#Функция поиска в логе команды открытия смены
def OpenShift():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    #Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        #если не нашли нужный текст в строке ищем дальше
        if oneline.find('<Response><Command>OpenShift') == -1 and oneline.find('<Response><Command>CloseShift') == -1:
            prev_string_oneline = oneline
            continue
        #если нашли
        else:
            #открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result =  open(TMP_result, 'a')
            #копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            #парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r')
            result = open(TMP_result, 'a')
            import xml.etree.ElementTree as ET
            tree = ET.ElementTree(file=TMP)
            for t in tree.iterfind('.//'):
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()

def get_ticketNum():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.ticketNum
        if oneline.find('<PreviewMode>') == -1:
            prev_string_oneline = oneline
            continue
        else:
            if oneline.find('<TicketNumMN>' + str(numbers)) == -1:
                prev_string_oneline = oneline
                continue
            else:
                # открываем текстовые файлы для вывода резельтатов поиска
                r = open(TMP, 'w')
                result = open(TMP_result, 'a')
                # копируем найденную строку в нужный файл
                result.write(prev_string_oneline + '\n')
                r.write(oneline + '\n' + '\n')
                r.close()
                result.close()
                # парсим XML по тегам и записываем результат в файл
                r = open(TMP, 'r', encoding="windows-1251")
                result = open(TMP_result, 'a', encoding="windows-1251")
                from lxml import etree as ET
                tree = ET.parse(r) # Парсинг строки
                for t in tree.iterfind('.//'):  # поиск элементов
                    temp = ('%s = %s' % (t.tag, t.text))
                    result.write(temp + '\n')
                r.close()
                result.close()

def get_error_code():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.error_code
        if oneline.find('<ErrorCode>' + str(numbers)) == -1:
            prev_string_oneline = oneline
            continue
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            # парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r', encoding="windows-1251")
            result = open(TMP_result, 'a', encoding="windows-1251")
            from lxml import etree as ET
            tree = ET.parse(r) # Парсинг строки
            for t in tree.iterfind('.//'):  # поиск элементов
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()

def get_CuState():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.error_code
        if oneline.find('<Response><Command>OpenShift') == -1 and oneline.find('<Response><Command>CloseShift') == -1 and oneline.find('<CuState>2<') == -1 and oneline.find('<CuState>1<') == -1:
            prev_string_oneline = oneline
            continue
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            # парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r', encoding="windows-1251")
            result = open(TMP_result, 'a', encoding="windows-1251")
            from lxml import etree as ET
            tree = ET.parse(r) # Парсинг строки
            for t in tree.iterfind('.//'):  # поиск элементов
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            r.close()
            result.close()

def get_FreeCellsCount():
        prev_string_oneline = ''
        f = open(filename, 'r', encoding="UTF-8")
        # Цикл построчного поиска нужной команды
        for oneline in f:
            oneline = oneline.strip(' \t\n\r ')
            # если не нашли нужный текст в строке ищем дальше
            numbers = namespace.error_code
            if oneline.find('<FreeCellsCount>10992<') == -1:
                prev_string_oneline = oneline
                continue
            else:
                # открываем текстовые файлы для вывода резельтатов поиска
                r = open(TMP, 'w')
                result = open(TMP_result, 'a')
                # копируем найденную строку в нужный файл
                result.write(prev_string_oneline + '\n')
                r.write(oneline + '\n' + '\n')
                r.close()
                result.close()
                # парсим XML по тегам и записываем результат в файл
                r = open(TMP, 'r', encoding="windows-1251")
                result = open(TMP_result, 'a', encoding="windows-1251")
                from lxml import etree as ET
                tree = ET.parse(r)  # Парсинг строки
                for t in tree.iterfind('.//'):  # поиск элементов
                    temp = ('%s = %s' % (t.tag, t.text))
                    result.write(temp + '\n')
                r.close()
                result.close()

def get_Test():
    prev_string_oneline = ''
    f = open(filename, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        # если не нашли нужный текст в строке ищем дальше
        numbers = namespace.error_code
        if oneline.find('<Response><Command>OpenShift') == -1 and oneline.find('<Response><Command>CloseShift') == -1 and oneline.find('<CuState>2<') == -1 and oneline.find('<CuState>1<') == -1:
            prev_string_oneline = oneline
            continue
        else:
            # открываем текстовые файлы для вывода резельтатов поиска
            r = open(TMP, 'w')
            result = open(TMP_result, 'a')
            # копируем найденную строку в нужный файл
            result.write(prev_string_oneline + '\n')
            r.write(oneline + '\n' + '\n')
            r.close()
            result.close()
            # парсим XML по тегам и записываем результат в файл
            r = open(TMP, 'r', encoding="windows-1251")
            result = open(TMP_result, 'a', encoding="windows-1251")
            from lxml import etree as ET
            tree = ET.parse(r) # Парсинг строки
            for t in tree.iterfind('.//'):  # поиск элементов
                temp = ('%s = %s' % (t.tag, t.text))
                result.write(temp + '\n')
            api = open(filename_2 , 'r', encoding='UTF-8')
            for oneline_api in api:
                oneline_api =  oneline_api.strip(' \t\n\r ')
                api_string = prev_string_oneline.split(":")[0] + ":" + prev_string_oneline.split(":")[1] + ":" + prev_string_oneline.split(":")[2]
                if oneline_api.find(api_string) == -1:
                   continue
                else:
                    result.write(oneline_api + '\n')
            r.close()
            result.close()



try:
    if namespace.OpenShift == True:
        OpenShift()
except Exception:
    print('Ошибка функции поиска OPEN SHIFT')
finally:
    print('', end='')

try:
    if namespace.ticketNum != 0:
        numbers = namespace.ticketNum
        get_ticketNum()
except Exception:
    print('Ошибка функции поиска Ticket Num')
finally:
    print('', end='')

try:
    if namespace.error_code != 0:
        numbers = namespace.error_code
        get_error_code()
except Exception:
    print('Ошибка функции поиска Error Code')
finally:
    print('', end='')

try:
    if namespace.CuState == True:
        get_CuState()
except Exception:
    print('Ошибка функции поиска CuState')
finally:
    print('', end='')

try:
    if namespace.FreeCellsCount == True:
        get_FreeCellsCount()
except Exception:
    print('Ошибка функции поиска Test')
finally:
    print('', end='')

try:
    if namespace.Test == True:
        get_Test()
except Exception:
    print('Ошибка функции поиска get_Test')
finally:
    print('', end='')

from os import startfile
startfile(TMP_result)

